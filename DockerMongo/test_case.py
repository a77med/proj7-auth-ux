import nose
import arrow
import acp_times

def test_open_time():
    start_time = arrow.now()
    assert acp_times.open_time(300, 1000, start_time.isoformat()) == start_time.shift(hours =+ 9, minutes =+ 0).isoformat()

def test_close_time():
    start_time = arrow.now()
    assert acp_times.close_time(200, 1000, start_time.isoformat()) == start_time.shift(hours =+ 13, minutes =+ 20).isoformat()

def test_finish_finish_line():
    start_time = arrow.now()
    assert acp_times.close_time(200, 200, start_time.isoformat()) == start_time.shift(hours =+ 13, minutes =+ 30).isoformat()
    
def test_zero_close_time():
    start_time = arrow.now()
    assert acp_times.close_time(0, 200, start_time.isoformat()) == start_time.shift(hours =+ 1, minutes =+ 0).isoformat()
    
def test_20_percent_case_close_time():
    start_time = arrow.now()
    assert acp_times.close_time(480, 400, start_time.isoformat()) == start_time.shift(hours =+ 27, minutes =+ 0).isoformat()

def test_above_20_percent_case():
    start_time = arrow.now()
    assert acp_times.close_time(481, 400, start_time.isoformat()) == start_time.shift(hours =+ 0, minutes =+ 0).isoformat()

def test_false():
    start_time = arrow.now()
    assert acp_times.close_time(720, 600, start_time.isoformat()) != start_time.shift(hours =+ 32, minutes =+ 10).isoformat()
