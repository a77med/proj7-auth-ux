# Laptop Service

#Libraries for password and flaskLogin

#from passlib.apps import custom_app_context as pwd_context
#from passlib.hash import pbkdf2_sha256
from flask_login import (LoginManager, current_user, login_required,
                            login_user, logout_user, UserMixin,
                            confirm_login, fresh_login_required)
#Libraries for token
from itsdangerous import (TimedJSONWebSignatureSerializer \
                                  as Serializer, BadSignature, \
                                  SignatureExpired)
import time


from flask_wtf import Form, CSRFProtect
from wtforms import BooleanField, StringField,PasswordField
from wtforms.validators import DataRequired

import flask_login
#previous Libraries
from flask_restful import Resource, Api
import flask
import os
from flask import Flask, request, Response, jsonify, redirect, url_for, render_template, abort, flash       
import pymongo
from pymongo import MongoClient
import csv
from password import hash_password, verify_password 
###                                                                                                                                                                                                   
# Globals                                                                                                                                                                                             
###                                                                                                                                                                                                  # Instantiate the app                                                                                                                                                                                 
app = Flask(__name__)
api = Api(app) 

#CONFIG = config.configuration()
app.config["SECRET_KEY"] = "SEARCHING for a Valid host"

#Creating a client object                                                                                                                                                                             
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)

#Connecting to the timesdb                                                                                                                                      
db = client.timesdb
#Creating and connecting userdb
dbu = client.userdb

#FROM flaskLogin.py


# step 1 in slides                                                                                                                                                                   
login_manager = LoginManager()
login_manager.init_app(app)
# step 6 in the slides                                                                                                                                                               
login_manager.login_view = "login"
login_manager.login_message = u"Please log in to access this page."
login_manager.refresh_view = "reauth"

# step 2 in slides                                                                                                                                                                
login_manager.setup_app(app)

# user class, defining required parameters
class User():
    
    def __init__(self, user_id):
        self.user_id = user_id
    def is_authenticated(self):
        return True
    def is_active(self):
        return True
    def is_anonymous(self):
        return False
    def get_id(self):
        return str(self.user_id)
    
    @staticmethod
    def validate_login(password,hashed_pass):
        return verify_password(password,hashed_pass)

    def generate_auth_token(self,secret,user_id,expiration):
        s = Serializer(secret, expires_in=expiration)
        # pass user ID
        return s.dumps(user_id)
    
            
@app.route("/")
def index():
    
    
    return redirect("/api/token")


class RegistrationForm(Form):
    username = StringField("username", validators=[DataRequired()])
    password = PasswordField("password", validators = [DataRequired()])

class LoginForm(Form):
    username = StringField("username", validators=[DataRequired()])
    password = PasswordField("password", validators = [DataRequired()])    
    remember = BooleanField("remember")
    
@app.route("/api/register", methods=["GET", "POST"])
def register():
    item_list = dbu.userdb.find()
    items = [item for item in item_list]
    form = RegistrationForm()
    
    
    if request.method == "POST" and form.validate:
        username = request.form.get("username")
        findUser = dbu.userdb.find_one( { "username": username } )
        if findUser:
            flash(u"Username Already exists.", "error"),400
            return render_template("register.html", form=form)
                
            
        else:
            password = request.form.get("password")
            #pass_hashed = pwd_context.hash(password)
            #pass_hashed = pbkdf2_sha256.hash(password)
            pass_hashed = hash_password(password)
            reg = {"username": username, "password": pass_hashed}
            dbu.userdb.insert_one(reg)
            new_user = dbu.userdb.find_one({ "username": username })
            result = { "username": new_user["username"], "password": new_user["password"], "_id": str(new_user["_id"]) }
            #result = {"username":username,"password":pass_hashed}
            return redirect("/api/token")
        
    else:
        flash(u"Please Fill the complete Form")
        return render_template("register.html", form=form)
    

# step 3 in slides
# This is one way. Using WTForms is another way.
@app.route("/api/token", methods=["GET", "POST"])
def login():
    item_list = dbu.userdb.find()
    items = [item for item in item_list]
    form = LoginForm()
    
    if request.method == "POST" and form.validate:
        username = request.form.get("username")
        password = request.form.get("password")
        remember = bool(request.form.get("remember"))
        findUser = dbu.userdb.find_one( { "username": username } )
        if not findUser:
            flash(u"Username does not exist.", "error"),400
            return render_template("login.html", form=form)


        elif User.validate_login(form.password.data,findUser['password']) == True:
            user_id = findUser['_id']
            id = { 'id' : str(user_id) }
            token = User(str(findUser['_id'])).generate_auth_token(app.config['SECRET_KEY'],id,600)
            token = token.decode('utf-8')
            result = { "token": token, "duration": 30, "remember": remember }
            next = request.args.get("next")
            if next != None:
                login_user(User(username), remember=remember)
                return redirect(next)
            login_user(User(username), remember=remember)
            return flask.jsonify(result=result)

        else:
            flash(u"Please check Username and password.")
            return render_template("login.html", form=form)
    
    else:
        flash(u"Please Fill the complete Form")
        return render_template("login.html", form=form)

# step 5 in slides
@login_manager.user_loader
def load_user(user_id):
    findUser = dbu.userdb.find_one({ "username": user_id })
    
    if not findUser:
        return None
    return User(findUser["_id"])

@app.route("/reauth", methods=["GET", "POST"])
@login_required
def reauth():
    if request.method == "POST":
        confirm_login()
        flash(u"Reauthenticated.")
        return redirect(request.args.get("next") or url_for("index"))
    return render_template("reauth.html")

@app.route("/logout")
@login_required
def logout():
    logout_user()
    flash("Logged out.")
    return redirect(url_for("index"))



#FROM testToken.py

def generate_auth_token(expiration=600):
    # s = Serializer(app.config['SECRET_KEY'], expires_in=expiration)
    s = Serializer('test1234@#$', expires_in=expiration)
    # pass index of user
    return s.dumps({'id': 1})

def verify_auth_token(token):
    s = Serializer('test1234@#$')
    try:
        data = s.loads(token)
    except SignatureExpired:
        return None    # valid token, but expired                                                                                                                                    
    except BadSignature:
        return None    # invalid token                                                                                                                                               
    return "Success"


#FROM password.py

#def hash_password(password):
#    return pwd_context.encrypt(password)

#def verify_password(password, hashVal):
#    return pwd_context.verify(password, hashVal)




################################################## END OF PROJECT 7

class listAll(Resource):
    @login_required
    def get(self):
        # if there is a top requested, get the integer
        top = request.args.get('top')
        # if there  is a string, return it as an int
        if top != None:
            top = int(top)
        # Basically unlimited, given our limited table
        else:
            top = 100
        # sorting the info in db to only pull open_time and close_time and pull it from
        # the Mongo DB
        item_find = db.timesdb.find().sort([("open_time",pymongo.ASCENDING),("close_time",pymongo.ASCENDING)]).limit(top)
        # Populate the list with the findings.
        item_list = [item for item in item_find]

        # Empty dict and lists (to be appended)
        time_dict = {}
        open_time = []
        close_time = []

        # Append the open_time and close_time items to the designated lists
        for item in item_list:
            open_time.append(item["open_time"])
            close_time.append(item["close_time"])

        # Adding key and values
        time_dict ["open_time"] = open_time
        # Adding key and values
        time_dict ["close_time"] = close_time

        return time_dict

# Create routes                                                                                                                                                                                      
# Another way, without decorators                                                                                                                                                                   
api.add_resource(listAll, '/listAll')



class listOpenOnly(Resource):
    @login_required
    def get(self):
	# if there is a top requested, get the integer
        top = request.args.get('top')
        # if there  is a string, return it as an int                                                                                                                                                 
        if top != None:
            top = int(top)
	# Basically unlimited, given our limited table                                                                                                                                               
        else:
            top = 100
	# sorting the info in db to only pull open_time and close_time and pull it from                                                                                                          
        # the Mongo DB                                                                                                                                                                               
        item_find = db.timesdb.find().sort([("open_time",pymongo.ASCENDING)]).limit(top)
	# Populate the list with the findings.                                                                                                                                                       
        item_list = [item for item in item_find]
        # Empty dict and lists (to be appended)                                                                                                                                                      
        time_dict = {}
        open_time = []
        close_time = []

        # Append the open_time and close_time items to the designated lists                                                                                                                          
        for item in item_list:
            open_time.append(item["open_time"])
           

	# Adding key and values                                                                                                                                                                      
        time_dict ["open_time"] = open_time
        # Adding key and values                                                                                                                                                                      
        #time_dict ["close_time"] = close_time

        return time_dict

# Create routes                                                                                                                                                                                      
# Another way, without decorators                                                                                                                                                                    
api.add_resource(listOpenOnly, '/listOpenOnly')




class listCloseOnly(Resource):
    @login_required
    def get(self):
	# if there is a top requested, get the integer                                                                                                                                               
        top = request.args.get('top')
        
        # if there  is a string, return it as an int                                                                                                                                                 
        if top != None:
            top = int(top)
            
	# Basically unlimited, given our limited table                                                                                                                                               
        else:
            top = 100
            
	# sorting the info in db to only pull open_time and close_time and pull it from                                                                                                              
        # the Mongo DB                                                                                                                                                                               
        item_find = db.timesdb.find().sort([("close_time",pymongo.ASCENDING)]).limit(top)
        
	# Populate the list with the findings.                                                                                                                                                       
        item_list = [item for item in item_find]

        # Empty dict and lists (to be appended)                                                                                                                                                      
        time_dict = {}
        open_time = []
        close_time = []

        # Append the open_time and close_time items to the designated lists                                                                                                                          
        for item in item_list:
            #open_time.append(item["open_time"])
            close_time.append(item["close_time"])

	# Adding key and values                                                                                                                                                                      
        #time_dict ["open_time"] = open_time
        # Adding key and values                                                                                                                                                                   
        time_dict ["close_time"] = close_time
        
        return time_dict

# Create routes                                                                                                                                                                                      
# Another way, without decorators                                                                                                                                                                    
api.add_resource(listCloseOnly, '/listCloseOnly')



class listAllcsv(Resource):
    @login_required
    def get(self):
	# if there is a top requested, get the integer                                                                                                                                               
        top = request.args.get('top')
        # if there  is a string, return it as an int                                                                                                                                                 
        if top != None:
            top = int(top)
            
	# Basically unlimited, given our limited table                                                                                                                                               
        else:
            top = 100
            
	# sorting the info in db to only pull open_time and close_time and pull it from                                                                                                              
        # the Mongo DB                                                                                                                                                                               
        item_find = db.timesdb.find().sort([("open_time",pymongo.ASCENDING),("close_time",pymongo.ASCENDING)]).limit(top)
	# Populate the list with the findings.                                                                                                                                                       
        item_list = [item for item in item_find]

        # Empty dict and lists (to be appended)                                                                                                                                                    
        open_time = []
        close_time = []
        csv_data = open("acp.csv","w")
        csv_write = csv.writer(csv_data)
        csv_write.writerow([["Open Time"],["Close Time"]])

        for item in item_list:
            csv_write.writerow([item["open_time"], item["close_time"]])

	# Adding key and values                                                                                                                                                                      
        csv_data = open("acp.csv","r")

        return Response(csv_data, mimetype = "text/csv")

# Create routes                                                                                                                                                                                      
# Another way, without decorators                                                                                                                                                                    
api.add_resource(listAllcsv, '/listAll/csv')


class listOpenOnlycsv(Resource):
    @login_required
    def get(self):
        # if there is a top requested, get the integer                                                                                                                                               
        top = request.args.get('top')

        # if there  is a string, return it as an int                                                                                                                                                 
        if top != None:
            top = int(top)

        else:
            top = 100

        # sorting the info in db to only pull open_time and close_time and pull it from                                                                                                              
        # the Mongo DB                                                                                                                                                                               
        item_find = db.timesdb.find().sort([("open_time",pymongo.ASCENDING)]).limit(top)

        # Populate the list with the findings.                                                                                                                                                       
        item_list = [item for item in item_find]

        # Empty dict and lists (to be appended)                                                                                                                                                       
        open_time = []
        close_time = []
        csv_data = open("acp.csv","w")
        csv_write = csv.writer(csv_data)
        csv_write.writerow(["Open Time"])

        for item in item_list:
            csv_write.writerow([item["open_time"]])

        # Adding key and values                                                                                                                                                                      
        csv_data = open("acp.csv","r")

        return Response(csv_data, mimetype = "text/csv")
        
# Create routes                                                                                                                                                                                      
# Another way, without decorators                                                                                                                                                                    
api.add_resource(listOpenOnlycsv, '/listOpenOnly/csv')
        
        
class listCloseOnlycsv(Resource):
    @login_required
    def get(self):
        # if there is a top requested, get the integer                                                                                                                                               
        top = request.args.get('top')
    
        # if there  is a string, return it as an int                                                                                                                                                 
        if top != None:
            top = int(top)
        
            # Basically unlimited, given our limited table                                                                                                                                               
        else:
            top = 100
            
        # sorting the info in db to only pull open_time and close_time and pull it from                                                                                                              
        # the Mongo DB                                                                                                                                                                               
        item_find = db.timesdb.find().sort([("close_time",pymongo.ASCENDING)]).limit(top)
        
        # Populate the list with the findings.                                                                                                                                                       
        item_list = [item for item in item_find]
        
        # Empty dict and lists (to be appended)                                                                                                                                                       
        open_time = []
        close_time = []
        csv_data = open("acp.csv","w")
        csv_write = csv.writer(csv_data)
        csv_write.writerow(["Close Time"])

        for item in item_list:
            csv_write.writerow([item["close_time"]])

        # Adding key and values                                                                                                                                                                      
        csv_data = open("acp.csv","r")

        return Response(csv_data, mimetype = "text/csv")

# Create routes                                                                                                                                                                                      
# Another way, without decorators                                                                                                                                                                    
api.add_resource(listCloseOnlycsv, '/listCloseOnly/csv')




class listAlljson(Resource):
    @login_required
    def get(self):
        # if there is a top requested, get the integer                                                                                                                                                
        top = request.args.get('top')
        # if there  is a string, return it as an int                                                                                                                                                 
        if top != None:
            top = int(top)

        else:
            top = 100
        # sorting the info in db to only pull open_time and close_time and pull it from                                                                                                               
        # the Mongo DB                                                                                                                                                                               
        item_find = db.timesdb.find().sort([("open_time",pymongo.ASCENDING),("close_time",pymongo.ASCENDING)]).limit(top)
        # Populate the list with the findings.                                                                                                                                                       
        item_list = [item for item in item_find]
        # Empty dict and lists (to be appended)                                                                                                                                                      
        time_dict = {}
        open_time = []
        close_time = []
        
        # Append the open_time and close_time items to the designated lists                                                                                                                          
        for item in item_list:
            open_time.append(item["open_time"])
            close_time.append(item["close_time"])
            
        # Adding key and values                                                                                                                                                                      
        time_dict ["open_time"] = open_time
        # Adding key and values                                                                                                                                                                      
        time_dict ["close_time"] = close_time
        
        return time_dict

# Create routes                                                                                                                                                                                      
# Another way, without decorators                                                                                                                                                                    
api.add_resource(listAlljson, '/listAll/json')


class listOpenOnlyjson(Resource):
    @login_required
    def get(self):
        # if there is a top requested, get the integer                                                                                                                                               
        top = request.args.get('top')
        # if there  is a string, return it as an int                                                                                                                                                 
        if top != None:
            top = int(top)
            
        else: 
            top = 100
        # sorting the info in db to only pull open_time and close_time and pull it from                                                                                                              
        # the Mongo DB                                                                                                                                                                               
        item_find = db.timesdb.find().sort([("open_time",pymongo.ASCENDING)]).limit(top)
        # Populate the list with the findings.                                                                                                                                                       
        item_list = [item for item in item_find]
        # Empty dict and lists (to be appended)                                                                                                                                                      
        time_dict = {}
        open_time = []
        close_time = []

        # Append the open_time and close_time items to the designated lists                                                                                                                          
        for item in item_list:
            open_time.append(item["open_time"])
            #close_time.append(item["close_time"])

        # Adding key and values                                                                                                                                                                      
        time_dict ["open_time"] = open_time
        # Adding key and values                                                                                                                                                                      
        #time_dict ["close_time"] = close_time

        return time_dict

# Create routes                                                                                                                                                                                      
# Another way, without decorators                                                                                                                                                                    
api.add_resource(listOpenOnlyjson, '/listOpenOnly/json')


class listCloseOnlyjson(Resource):
    @login_required
    def get(self):
        # if there is a top requested, get the integer                                                                                                                                                
        top = request.args.get('top')
        # if there  is a string, return it as an int                                                                                                                                                 
        if top != None:
            top = int(top)
            # Basically unlimited, given our limited table                                                                                                                                    
        else:
            top = 100
        # sorting the info in db to only pull open_time and close_time and pull it from                                                                                                               
        # the Mongo DB                                                                                                                                                                               
        item_find = db.timesdb.find().sort([("close_time",pymongo.ASCENDING)]).limit(top)
        # Populate the list with the findings.                                                                                                                                                       
        item_list = [item for item in item_find]
        # Empty dict and lists (to be appended)                                                                                                                                                      
        time_dict = {}
        open_time = []
        close_time = []

        # Append the open_time and close_time items to the designated lists                                                                                                                          
        for item in item_list:
            #open_time.append(item["open_time"])
            close_time.append(item["close_time"])

        # Adding key and values                                                                                                                                                                      
        #time_dict ["open_time"] = open_time
        # Adding key and values                                                                                                                                                                      
        time_dict ["close_time"] = close_time

        return time_dict

# Create routes                                                                                                                                                                                      
# Another way, without decorators                                                                                                                                                                    
api.add_resource(listCloseOnlyjson, '/listCloseOnly/json')


# Run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)
