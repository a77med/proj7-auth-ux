#Project 7
#Author: Ahmed Al Ali (aalali@uoregon.edu)

Functionalities implemented..

1)going to localhost:5001 will show a login form.
The login form also has a remember me button and a register here redirection.

2)registering at localhost:5001/api/register
A registration form is created. check if username exists, and if fields arent empty.
If all above meets requirements (being a unique username. fields are not empty) the
new user will be created under userDB.

3)Logging in at localhost:5001/api/token
Logs in user and gives the user persmission to access php files created for project 6.
Login is required for accessing everything to do with proj6.

Docker builds fine with the docker-compose.yml given.

Thank you for reading me.